package com.rdzdevelopments.bestsecret.domain.repositories

import com.rdzdevelopments.bestsecret.data.error.MyResult
import com.rdzdevelopments.bestsecret.data.model.Product
import com.rdzdevelopments.bestsecret.data.model.ProductsResponse

interface ProductRepository {

    suspend fun getProducts(page : Int, pageSize : Int) : MyResult<ProductsResponse?>

    suspend fun getProductById(id : Int) : MyResult<Product?>

}