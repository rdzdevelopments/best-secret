package com.rdzdevelopments.bestsecret.domain.usercases

import com.rdzdevelopments.bestsecret.domain.repositories.ProductRepository

class GetProductDetailUseCase(private val repository : ProductRepository) {

    suspend fun execute(id : Int) = repository.getProductById(id)
}