package com.rdzdevelopments.bestsecret.domain.usercases

import com.rdzdevelopments.bestsecret.domain.repositories.ProductRepository

class GetProductsUseCase(private val repository : ProductRepository) {

    suspend fun execute(page : Int, pageSize : Int) = repository.getProducts(page, pageSize)
}