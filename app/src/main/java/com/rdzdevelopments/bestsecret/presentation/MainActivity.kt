package com.rdzdevelopments.bestsecret.presentation

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.rdzdevelopments.bestsecret.R
import com.rdzdevelopments.bestsecret.databinding.MainActivityBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding : MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
        configOrientation()
    }

    private fun configOrientation() {
        requestedOrientation = if(resources.getBoolean(R.bool.portrait_only)){
            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
        else{
            ActivityInfo.SCREEN_ORIENTATION_SENSOR
        }
    }

    fun getToolbar() = binding.toolbar

}