package com.rdzdevelopments.bestsecret.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rdzdevelopments.bestsecret.R
import com.rdzdevelopments.bestsecret.data.model.Product
import com.rdzdevelopments.bestsecret.databinding.ProductAdapterBinding

class ProductAdapter(private val onItemClick : (Int) -> Unit, private val onAddCartClick : (Int) -> Unit) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private val productList = mutableListOf<Product>()

    fun clearProducts(){
        productList.clear()
        notifyItemRangeRemoved(0, productList.size)
    }

    fun insertProducts(products : List<Product>){
        productList.addAll(products)
        notifyItemRangeInserted(productList.size, products.size)
    }

    inner class ProductViewHolder(private val binding : ProductAdapterBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val productId = productList[adapterPosition].id
                onItemClick(productId)
            }

            binding.cardButton.setOnClickListener{
                val productId = productList[adapterPosition].id
                onAddCartClick(productId)
            }
        }

        fun bind(position: Int){
            val product = productList[position]
            binding.image.setImage(product.image)
        }
    }

    fun ImageView.setImage(url : String?){
        Glide
            .with(context)
            .load(url)
            .placeholder(R.drawable.shop_placeholder)
            .into(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ProductAdapterBinding>(layoutInflater, R.layout.product_adapter, parent, false)
        return ProductViewHolder(binding)
    }

    override fun getItemCount() = productList.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(position)
    }
}