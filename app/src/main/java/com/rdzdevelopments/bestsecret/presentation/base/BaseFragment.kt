package com.rdzdevelopments.bestsecret.presentation.base

import android.app.AlertDialog
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.rdzdevelopments.bestsecret.R

abstract class BaseFragment : Fragment() {

    var fragmentId: String = javaClass.name

    fun ImageView.setImage(url : String?){
        Glide
            .with(context)
            .load(url)
            .placeholder(R.drawable.shop_placeholder)
            .into(this)
    }

    fun addFragment(frame: Int, fragment: BaseFragment, addToBackStack: Boolean = false) {
        parentFragmentManager.beginTransaction()
            .add(frame, fragment)
            .also {
                if (addToBackStack) {
                    it.addToBackStack(fragmentId)
                }
            }
            .commit()
    }

    fun showDialog(string : String) {
        AlertDialog.Builder(requireActivity())
        .setMessage(string)
        .create()
        .show()
    }

    fun showNotImplementedDialog(){
        showDialog("This feature is not implemented yet. Stay tuned, it's coming soon!")
    }

    fun loadLayoutOrientation() {
        try {
            parentFragmentManager
                .beginTransaction()
                .setReorderingAllowed(false)
                .detach(this)
                .attach(this)
                .commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}