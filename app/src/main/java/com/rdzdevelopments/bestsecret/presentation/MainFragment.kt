package com.rdzdevelopments.bestsecret.presentation

import android.content.res.Configuration
import android.os.Bundle
import android.view.*
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING
import com.rdzdevelopments.bestsecret.R
import com.rdzdevelopments.bestsecret.databinding.MainFragmentBinding
import com.rdzdevelopments.bestsecret.presentation.base.BaseFragment
import org.koin.android.viewmodel.ext.android.sharedViewModel


class MainFragment : BaseFragment() {

    private lateinit var binding: MainFragmentBinding
    private val mainViewModel: MainViewModel by sharedViewModel()
    val isLoading = MutableLiveData<Boolean>(false)
    private var hasRotated = false

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        hasRotated = mainViewModel.hasChangedConfiguration(newConfig.orientation)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configToolbar()
    }

    private fun configToolbar() {
        with((requireActivity() as MainActivity).getToolbar()){
            title = getString(R.string.products)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        binding.fragment = this
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!hasRotated){
            initContainer()
            mainViewModel.resetPageProducts()
            loadProducts()
            observeErrors()
        }
    }

    private fun observeErrors() {
        mainViewModel.errorObserver.observe(viewLifecycleOwner, Observer {
                isLoading.value = false
                showDialog(it)
        })
    }

    private fun initContainer() = with(binding.container){
        layoutManager = GridLayoutManager(requireContext(), 2)
        adapter = ProductAdapter( { loadProductById(it) }, { showNotImplementedDialog() } )
        configContainer()
    }

    private fun configContainer() {
        binding.container.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState != SCROLL_STATE_DRAGGING && !recyclerView.canScrollVertically(1) && !isLoading.value!!) {
                    mainViewModel.setNextPage()
                    loadProducts()
                }
            }
        })
    }

    private fun loadProductById(id: Int) {
        if(isLoading.value == false){
            isLoading.value = true
            mainViewModel.getProductById(id)
            mainViewModel.selectedProduct.observe(viewLifecycleOwner, Observer { prod ->
                prod?.let {
                    goToProductDetailScreen()
                }
                isLoading.value = false
            })
        }
    }

    private fun goToProductDetailScreen() {
        addFragment(frame = R.id.root_layout, fragment = ProductDetailFragment(), addToBackStack = true)
    }

    private fun loadProducts(){
        isLoading.value = true
        getProducts()
    }

    private fun getProducts() {
        mainViewModel.getProducts()
        mainViewModel.productList.observe(viewLifecycleOwner, Observer{
            (binding.container.adapter as ProductAdapter).insertProducts(it)
            isLoading.value = false
        })
    }
}