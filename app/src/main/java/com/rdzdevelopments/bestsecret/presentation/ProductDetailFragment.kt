package com.rdzdevelopments.bestsecret.presentation

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rdzdevelopments.bestsecret.databinding.ProductDetailFragmentBinding
import com.rdzdevelopments.bestsecret.presentation.base.BaseFragment
import org.koin.android.viewmodel.ext.android.sharedViewModel


class ProductDetailFragment : BaseFragment() {

    private lateinit var binding : ProductDetailFragmentBinding
    private val mainViewModel: MainViewModel by sharedViewModel()

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        loadLayoutOrientation()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideActivityToolbar()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = ProductDetailFragmentBinding.inflate(inflater, container, false)
        binding.fragment = this
        binding.product = mainViewModel.selectedProduct.value
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProductImage()
        configToolbar()
    }

    private fun hideActivityToolbar() {
        with((requireActivity() as MainActivity).getToolbar()){
            visibility = View.GONE
        }
    }

    private fun configToolbar() {
        binding.toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
    }

    private fun setProductImage() {
        val productImage = mainViewModel.selectedProduct.value?.image
        binding.productImageview.setImage(productImage)
    }

    fun addToCartOnClick(){
        showNotImplementedDialog()
    }

    override fun onDestroy() {
        super.onDestroy()
        showActivityToolbar()
    }

    private fun showActivityToolbar() {
        with((requireActivity() as MainActivity).getToolbar()){
            visibility = View.VISIBLE
        }
    }

    fun getDiscountPrice(price : Double, discountPercentage : Double) = mainViewModel.getDiscountPrice(price, discountPercentage)


}