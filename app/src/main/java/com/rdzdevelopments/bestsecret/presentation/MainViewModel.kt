package com.rdzdevelopments.bestsecret.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.rdzdevelopments.bestsecret.data.error.MyResult
import com.rdzdevelopments.bestsecret.data.model.Product
import com.rdzdevelopments.bestsecret.domain.usercases.GetProductDetailUseCase
import com.rdzdevelopments.bestsecret.domain.usercases.GetProductsUseCase

class MainViewModel(
    private val getProductsUseCase: GetProductsUseCase,
    private val getProductDetailUseCase: GetProductDetailUseCase
) : ViewModel() {

    companion object{
        const val PRODUCTS_PER_PAGE = 5
    }

    lateinit var selectedProduct : LiveData<Product?>
    lateinit var productList : LiveData<List<Product>>


    var currentPage = 1
        private set

    var errorHandler = MutableLiveData<String>()
    var errorObserver : LiveData<String> = errorHandler
    var phoneOrientation : Int? = null



    fun getProducts() {
        productList = liveData {
            val response = getProductsUseCase.execute(currentPage, PRODUCTS_PER_PAGE)
            when (response) {
                is MyResult.Success -> emit(response.data?.list ?: listOf<Product>())
                is MyResult.Error -> errorHandler.postValue(response.error.userMessage)
            }
        }
    }


    fun getProductById(id : Int){
        selectedProduct = liveData {
            val response = getProductDetailUseCase.execute(id = id)
            when(response){
                is MyResult.Success -> emit(response.data)
                is MyResult.Error -> errorHandler.value = response.error.userMessage
            }
        }
    }

    fun resetPageProducts() {
        currentPage = 1
    }

    fun setNextPage() {
        currentPage++
    }

    fun hasChangedConfiguration(newOrientation : Int): Boolean {
        return if (phoneOrientation == null){
            phoneOrientation = newOrientation
            false
        }
        else{
            phoneOrientation != newOrientation
        }
    }

    fun getDiscountPrice(price : Double, discountPercentage : Double) = (price - (discountPercentage * price / 100)).toString()
}