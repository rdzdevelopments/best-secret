package com.rdzdevelopments.bestsecret.data.repositories.datasource

import com.rdzdevelopments.bestsecret.data.model.Product
import com.rdzdevelopments.bestsecret.data.model.ProductsResponse
import retrofit2.Response

interface ProductRemoteData {

    suspend fun getProducts(page : Int, pageSize : Int) : Response<ProductsResponse>?

    suspend fun getProductById(productId : Int) : Response<Product>?

}