package com.rdzdevelopments.bestsecret.data.model

data class Product (
    val _link: String,
    val _type: String,
    val brand: String,
    val description: String,
    val discountPercentage: Double,
    val currency: String,
    val id: Int,
    val image: String,
    val name: String,
    val price: Double,
    val stock: Int
)