package com.rdzdevelopments.bestsecret.data.model

data class ProductsResponse(
    val _link: String,
    val _next: String,
    val _type: String,
    val list: List<Product>,
    val page: Int,
    val pageSize: Int,
    val size: String
)