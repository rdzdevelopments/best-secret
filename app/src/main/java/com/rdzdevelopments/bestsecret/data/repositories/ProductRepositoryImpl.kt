package com.rdzdevelopments.bestsecret.data.repositories

import com.google.gson.Gson
import com.rdzdevelopments.bestsecret.data.error.ErrorResponse
import com.rdzdevelopments.bestsecret.data.error.MyResult
import com.rdzdevelopments.bestsecret.data.model.Product
import com.rdzdevelopments.bestsecret.data.model.ProductsResponse
import com.rdzdevelopments.bestsecret.data.repositories.datasource.ProductRemoteData
import com.rdzdevelopments.bestsecret.domain.repositories.ProductRepository

class ProductRepositoryImpl(private val remoteData : ProductRemoteData) : ProductRepository {

    override suspend fun getProducts(page : Int, pageSize : Int) : MyResult<ProductsResponse?> {
        val response = remoteData.getProducts(page = page, pageSize = pageSize)
        return if(response?.isSuccessful == true){
            MyResult.Success(response.body())
        }
        else{
            val errorResponse = Gson().fromJson(response?.errorBody()?.string(), ErrorResponse::class.java)
            MyResult.Error(errorResponse)
        }
    }

    override suspend fun getProductById(id: Int) : MyResult<Product?>{
        val response = remoteData.getProductById(productId = id)
        return if(response?.isSuccessful == true){
            MyResult.Success(response.body())
        }
        else{
            val errorResponse = Gson().fromJson(response?.errorBody()?.string(), ErrorResponse::class.java)
            MyResult.Error(errorResponse)
        }
    }

}