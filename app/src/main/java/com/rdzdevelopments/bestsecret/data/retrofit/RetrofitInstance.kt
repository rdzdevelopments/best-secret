package com.rdzdevelopments.bestsecret.data.retrofit

import com.google.gson.GsonBuilder
import com.rdzdevelopments.bestsecret.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitInstance {

    private val interceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private val client = OkHttpClient.Builder().apply {
        addInterceptor(interceptor)
    }.build()


    private fun getRetrofitInstance(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.URL_BASE)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(client)
            .build()

    fun getRetrofitService(): APImethods =
        getRetrofitInstance()
            .create(APImethods::class.java)

}