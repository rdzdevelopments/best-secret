package com.rdzdevelopments.bestsecret.data.retrofit

import com.rdzdevelopments.bestsecret.BuildConfig
import com.rdzdevelopments.bestsecret.data.model.Product
import com.rdzdevelopments.bestsecret.data.model.ProductsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface APImethods {

    @GET("products")
    suspend fun getProducts(
        @Header("Authorization") authHeader : String = BuildConfig.API_AUTH,
        @Query("page") page : Int,
        @Query("pageSize") pageSize : Int
    ) : Response<ProductsResponse>

    @GET("products/{productId}")
    suspend fun getProductById(
        @Header("Authorization") authHeader : String = BuildConfig.API_AUTH,
        @Path("productId") productId : Int,
    ) : Response<Product>

}