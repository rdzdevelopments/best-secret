package com.rdzdevelopments.bestsecret.data.repositories.datasource

import com.rdzdevelopments.bestsecret.data.retrofit.APImethods

class ProductRemoteDataImpl(private val api : APImethods) : ProductRemoteData {

    override suspend fun getProducts(page : Int, pageSize : Int) = api.getProducts(page = page, pageSize = pageSize)

    override suspend fun getProductById(id: Int) = api.getProductById(productId = id)

}