package com.rdzdevelopments.bestsecret.data.error

sealed class MyResult<T> {

    data class Success<T>(val data: T) : MyResult<T>()

    data class Error<T>(val error: ErrorResponse) : MyResult<T>()
}
