package com.rdzdevelopments.bestsecret.data.error

data class ErrorResponse(
    val _type: String,
    val developerMessage: String,
    val httpStatusCode: Int,
    val internalErrorCode: String,
    val userMessage: String
)