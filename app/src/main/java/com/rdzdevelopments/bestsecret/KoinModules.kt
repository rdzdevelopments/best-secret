package com.rdzdevelopments.bestsecret

import com.rdzdevelopments.bestsecret.data.repositories.ProductRepositoryImpl
import com.rdzdevelopments.bestsecret.data.repositories.datasource.ProductRemoteData
import com.rdzdevelopments.bestsecret.data.repositories.datasource.ProductRemoteDataImpl
import com.rdzdevelopments.bestsecret.data.retrofit.APImethods
import com.rdzdevelopments.bestsecret.data.retrofit.RetrofitInstance
import com.rdzdevelopments.bestsecret.domain.repositories.ProductRepository
import com.rdzdevelopments.bestsecret.domain.usercases.GetProductDetailUseCase
import com.rdzdevelopments.bestsecret.domain.usercases.GetProductsUseCase
import com.rdzdevelopments.bestsecret.presentation.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single<APImethods>{ RetrofitInstance.getRetrofitService() }
    single<ProductRemoteData> { ProductRemoteDataImpl(get()) }
    single<ProductRepository>{ ProductRepositoryImpl(get()) }

    factory<GetProductDetailUseCase> { GetProductDetailUseCase(get()) }
    factory<GetProductsUseCase> { GetProductsUseCase(get()) }

    viewModel { MainViewModel(get(), get())}

}