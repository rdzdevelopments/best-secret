package com.rdzdevelopments.bestsecret

import android.app.Activity
import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.rdzdevelopments.bestsecret.presentation.MainActivity
import com.rdzdevelopments.bestsecret.utils.RecyclerViewMatcher
import org.junit.After
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
abstract class BaseTest {

    private lateinit var scenario: ActivityScenario<MainActivity>


    fun waitingForServerAnswer(milis: Long) {
        Thread.sleep(milis)
    }

    fun getResourceString(id: Int) = InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(id)


    fun <T : Activity> setScenario(act: Class<T>) {
        scenario = ActivityScenario.launch(Intent(ApplicationProvider.getApplicationContext(), act))
    }

    @After
    fun cleanup() {
        scenario.close()
    }


    fun withRecyclerView(recyclerViewId : Int) : RecyclerViewMatcher {
        return RecyclerViewMatcher(
            recyclerViewId
        )
    }
}
