package com.rdzdevelopments.bestsecret

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.rdzdevelopments.bestsecret.presentation.MainActivity
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.lang.Exception


@RunWith(JUnit4::class)
class MainTest : BaseTest() {

    @Before
    fun initScenario(){
        setScenario(MainActivity::class.java)
    }

    @Test
    fun addCartTestOnMultipleItems() {
        waitingForServerAnswer(3000)
        testOnSingleItem(0)
        testOnSingleItem(1)
    }

    @Test
    fun addCartTestOnDetailScreen() {
        waitingForServerAnswer(2500)
        clickOnProduct(1)
        clickOnProduct(0)
    }

    private fun testOnDetailedItem() {
        try {
            onView(withId(R.id.add_or_notify_button)).perform(ViewActions.click())
            waitingForServerAnswer(100)
            checkDialogPopUp()
        }catch (e : Exception){
            onView(withId(android.R.id.message)).check(matches(isDisplayed()))
            pressBack()
        }
    }

    private fun clickOnProduct(pos : Int) {
        onView(
            withRecyclerView(R.id.container)
                .atPositionOnView(pos, R.id.item)
        ).perform(
            ViewActions.click()
        )
        waitingForServerAnswer(200)
        testOnDetailedItem()
    }


    private fun testOnSingleItem(position : Int){
        onView(
            withRecyclerView(R.id.container)
                .atPositionOnView(position, R.id.card_button)
        ).perform(
            ViewActions.click()
        )

       checkDialogPopUp()
    }

    private fun checkDialogPopUp() {
        onView(withText("This feature is not implemented yet. Stay tuned, it's coming soon!")).check(matches(isDisplayed()))
        waitingForServerAnswer(100)
        pressBack()
        waitingForServerAnswer(100)
    }
}
